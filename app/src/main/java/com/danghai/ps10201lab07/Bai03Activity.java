package com.danghai.ps10201lab07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Bai03Activity extends AppCompatActivity {

    Button btnRun, btnReset;
    ImageView hour, minute, second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai03);
        btnRun = findViewById(R.id.btn_run);
        btnReset = findViewById(R.id.btn_reset);

        hour = findViewById(R.id.clock_hour);
        minute = findViewById(R.id.clock_minute);
        second = findViewById(R.id.clock_second);
//        Timer timer = new Timer();
//        final int FPS = 40;
//        updateTimeTask updateTime = new updateTimeTask();
//        timer.schedule(updateTime, 0, 1000/FPS);



        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int h = 0;
                int m = 0;
                int s = 0;
                ObjectAnimator animator2 = ObjectAnimator.ofFloat(hour, "rotation", h);
                animator2.setDuration(2000);
                animator2.start();
                ObjectAnimator animator = ObjectAnimator.ofFloat(minute, "rotation", m);
                animator.setDuration(2000);
                animator.start();
                ObjectAnimator animator1 = ObjectAnimator.ofFloat(second, "rotation", s);
                animator1.setDuration(2000);
                animator1.start();
            }
        });


        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SimpleDateFormat formatter= new SimpleDateFormat("hh:mm:ss");
                String dateTime = formatter.format(new Date());
                String[] part = dateTime.split(":");
                final int hours = Integer.valueOf(part[0]);
                final int minutes = Integer.valueOf(part[1]);
                final int seconds = Integer.valueOf(part[2]);
                int h = 0;
                int m = 30;
                int s = 150;
                switch (hours){
                    case 1:
                        h = 30;
                        break;
                    case 2:
                        h = 60;
                        break;
                    case 3:
                        h = 90;
                        break;
                    case 4:
                        h = 120;
                        break;
                    case 5:
                        h = 150;
                        break;
                    case 6:
                        h= 180;
                        break;
                    case 7:
                        h = 210;
                        break;
                    case 8:
                        h = 240;
                        break;
                    case 9:
                        h = 270;
                        break;
                    case 10:
                        h = 300;
                    case 11:
                        h = 330;
                    case 12:
                        h = 0;
                }



                switch (minutes){
                    case 1:
                        m = 6;
                        break;
                    case 2:
                        m = 12;
                        break;
                    case 3:
                        m = 18;
                        break;
                    case 4:
                        m = 24;
                        break;
                    case 5:
                        m = 30;
                        break;
                    case 6:
                        m = 36;
                        break;
                    case 7:
                        m = 42;
                        break;
                    case 8:
                        m = 48;
                        break;
                    case 9:
                        m = 52;
                        break;
                    case 10:
                        m = 60;
                        break;
                    case 11:
                        m = 66;
                        break;
                    case 12:
                        m = 72;
                        break;
                    case 13:
                        m = 78;
                        break;
                    case 14:
                        m = 84;
                        break;
                    case 15:
                        m = 90;
                        break;
                    case 16:
                        m = 96;
                        break;
                    case 17:
                        m = 102;
                        break;
                    case 18:
                        m = 108;
                        break;
                    case 19:
                        m = 114;
                        break;
                    case 20:
                        m = 120;
                        break;
                    case 21:
                        m = 126;
                        break;
                    case 22:
                        m = 132;
                        break;
                    case 23:
                        m = 138;
                        break;
                    case 24:
                        m = 144;
                        break;
                    case 25:
                        m = 150;
                        break;
                    case 26:
                        m = 156;
                        break;
                    case 27:
                        m = 162;
                        break;
                    case 28:
                        m = 168;
                        break;
                    case 29:
                        m = 174;
                        break;
                    case 30:
                        m = 180;
                        break;
                    case 31:
                        m = 186;
                        break;
                    case 32:
                        m = 192;
                        break;
                    case 33:
                        m = 198;
                        break;
                    case 34:
                        m = 204;
                        break;
                    case 35:
                        m = 210;
                        break;
                    case 36:
                        m = 216;
                        break;
                    case 37:
                        m = 222;
                        break;
                    case 38:
                        m = 228;
                        break;
                    case 39:
                        m = 234;
                        break;
                    case 40:
                        m = 240;
                        break;
                    case 41:
                        m = 246;
                        break;
                    case 42:
                        m = 252;
                        break;
                    case 43:
                        m = 258;
                        break;
                    case 44:
                        m = 264;
                        break;
                    case 45:
                        m = 270;
                        break;
                    case 46:
                        m = 276;
                        break;
                    case 47:
                        m = 282;
                        break;
                    case 48:
                        m = 288;
                        break;
                    case 49:
                        m = 294;
                        break;
                    case 50:
                        m = 300;
                        break;
                    case 51:
                        m = 306;
                        break;
                    case 52:
                        m = 312;
                        break;
                    case 53:
                        m = 318;
                        break;
                    case 54:
                        m = 324;
                        break;
                    case 55:
                        m = 330;
                        break;
                    case 56:
                        m = 336;
                        break;
                    case 57:
                        m = 342;
                        break;
                    case 58:
                        m = 348;
                        break;
                    case 59:
                        m = 352;
                        break;
                    case 60:
                        m = 0;
                        break;
                }
                switch (seconds){
                    case 1:
                        s = 6;
                        break;
                    case 2:
                        s = 12;
                        break;
                    case 3:
                        s = 18;
                        break;
                    case 4:
                        s = 24;
                        break;
                    case 5:
                        s = 30;
                        break;
                    case 6:
                        s = 36;
                        break;
                    case 7:
                        s = 42;
                        break;
                    case 8:
                        s = 48;
                        break;
                    case 9:
                        s = 52;
                        break;
                    case 10:
                        s = 60;
                        break;
                    case 11:
                        s = 66;
                        break;
                    case 12:
                        s = 72;
                        break;
                    case 13:
                        s = 78;
                        break;
                    case 14:
                        s = 84;
                        break;
                    case 15:
                        s = 90;
                        break;
                    case 16:
                        s = 96;
                        break;
                    case 17:
                        s = 102;
                        break;
                    case 18:
                        s = 108;
                        break;
                    case 19:
                        s = 114;
                        break;
                    case 20:
                        s = 120;
                        break;
                    case 21:
                        s = 126;
                        break;
                    case 22:
                        s = 132;
                        break;
                    case 23:
                        s = 138;
                        break;
                    case 24:
                        s = 144;
                        break;
                    case 25:
                        s = 150;
                        break;
                    case 26:
                        s = 156;
                        break;
                    case 27:
                        s = 162;
                        break;
                    case 28:
                        s = 168;
                        break;
                    case 29:
                        s = 174;
                        break;
                    case 30:
                        s = 180;
                        break;
                    case 31:
                        s = 186;
                        break;
                    case 32:
                        s = 192;
                        break;
                    case 33:
                        s = 198;
                        break;
                    case 34:
                        s = 204;
                        break;
                    case 35:
                        s = 210;
                        break;
                    case 36:
                        s = 216;
                        break;
                    case 37:
                        s = 222;
                        break;
                    case 38:
                        s = 228;
                        break;
                    case 39:
                        s = 234;
                        break;
                    case 40:
                        s = 240;
                        break;
                    case 41:
                        s = 246;
                        break;
                    case 42:
                        s = 252;
                        break;
                    case 43:
                        s = 258;
                        break;
                    case 44:
                        s = 264;
                        break;
                    case 45:
                        s = 270;
                        break;
                    case 46:
                        s = 276;
                        break;
                    case 47:
                        s = 282;
                        break;
                    case 48:
                        s = 288;
                        break;
                    case 49:
                        s = 294;
                        break;
                    case 50:
                        s = 300;
                        break;
                    case 51:
                        s = 306;
                        break;
                    case 52:
                        s = 312;
                        break;
                    case 53:
                        s = 318;
                        break;
                    case 54:
                        s = 324;
                        break;
                    case 55:
                        s = 330;
                        break;
                    case 56:
                        s = 336;
                        break;
                    case 57:
                        s = 342;
                        break;
                    case 58:
                        s = 348;
                        break;
                    case 59:
                        s = 352;
                        break;
                    case 60:
                        s = 0;
                        break;
                }

                ObjectAnimator animator2 = ObjectAnimator.ofFloat(hour, "rotation", h);
                animator2.setDuration(2000);
                animator2.start();
                ObjectAnimator animator = ObjectAnimator.ofFloat(minute, "rotation", m);
                animator.setDuration(2000);
                animator.start();
                ObjectAnimator animator1 = ObjectAnimator.ofFloat(second, "rotation", s);
                animator1.setDuration(2000);
                animator1.start();
                Log.d("seconds", seconds + "");

            }
        });


    }

//    class updateTimeTask extends TimerTask{
//
//        @Override
//        public void run() {
//
//
//        }
//    }
}
