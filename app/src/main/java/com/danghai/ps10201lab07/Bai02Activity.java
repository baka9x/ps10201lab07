package com.danghai.ps10201lab07;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Bai02Activity extends AppCompatActivity {

    Button btnAll, btnDoraemon, btnNobita;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai02);

        btnAll = findViewById(R.id.btn_all);
        btnDoraemon = findViewById(R.id.btn_doraemon);
        btnNobita = findViewById(R.id.btn_nobita);

        imageView = findViewById(R.id.image_view);


        btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("All");
            }
        });

        btnDoraemon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Doraemon");
            }
        });

        btnNobita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImage("Nobita");
            }
        });
    }

    private void showImage(String img){
        //Hide image
        ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "translationX", 0f, 500f);
        animator.setDuration(2000);
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(imageView, "alpha", 1f, 0f);
        animator1.setDuration(2000);

        //Show image
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(imageView, "translationX", -500f, 0f);
        animator2.setDuration(2000);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        animator2.setDuration(2000);

        //Config slide show process to show next image
        AnimatorSet animatorSet = new AnimatorSet();

        animatorSet.play(animator2).with(animator3).after(animator).after(animator1);
        animatorSet.start();

        final String imageName = img;
        //Set listener for determine when animation finished

        animator1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {


                //Change source of ImageView
                if (imageName.equals("Nobita")){
                    imageView.setImageResource(R.drawable.nobita);
                }
                if (imageName.equals("Doraemon")){
                    imageView.setImageResource(R.drawable.doraemon);
                }
                if (imageName.equals("All")){
                    imageView.setImageResource(R.drawable.all);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });


    }
}
