package com.danghai.ps10201lab07;

import android.animation.ObjectAnimator;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.PathInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.nio.file.Path;

public class Bai01Activity extends AppCompatActivity {

    ImageView imageView;

    Button btnRotation, btnMoving, btnZoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai01);


        imageView = findViewById(R.id.image_view);
        btnRotation = findViewById(R.id.btn_rotation);
        btnMoving = findViewById(R.id.btn_moving);
        btnZoom = findViewById(R.id.btn_zoom);

        //Rotation image
        btnRotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int dest = 360;
                if (imageView.getRotation() == 360) {
                    System.out.println(imageView.getAlpha());
                    dest = 0;
                }
                ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "rotation", dest);
                animator.setDuration(2000);
                animator.start();

            }
        });
        //Moving image
        btnMoving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int dest = 360;
                ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "translationY", dest);
                animator.setDuration(2000);
                animator.start();
            }
        });
        //Zoom image
        btnZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Animation animation = AnimationUtils.loadAnimation(Bai01Activity.this, R.anim.zoom);
                imageView.startAnimation(animation);
            }
        });
    }
}
